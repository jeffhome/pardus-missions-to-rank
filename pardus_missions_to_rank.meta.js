// ==UserScript==
// @name        Missions To Rank
// @namespace   http://userscripts.xcom-alliance.info/
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.0
// @description Shows the number of missions completed and number remaining until the next mission in the Stats Overview page.
// @include     http*://*.pardus.at/overview_stats.php
// @updateURL   http://userscripts.xcom-alliance.info/missions_to_rank/pardus_missions_to_rank.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/missions_to_rank/pardus_missions_to_rank.user.js
// @icon        http://userscripts.xcom-alliance.info/missions_to_rank/icon.png
// ==/UserScript==
