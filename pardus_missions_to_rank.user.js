// ==UserScript==
// @name        Missions To Rank
// @namespace   http://userscripts.xcom-alliance.info/
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.0
// @description Shows the number of missions completed and number remaining until the next mission in the Stats Overview page.
// @include     http*://*.pardus.at/overview_stats.php
// @updateURL   http://userscripts.xcom-alliance.info/missions_to_rank/pardus_missions_to_rank.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/missions_to_rank/pardus_missions_to_rank.user.js
// @icon        http://userscripts.xcom-alliance.info/missions_to_rank/icon.png
// ==/UserScript==

var msns=[0,8,23,46,83,133,201,288,397,530,690,878,1295];
var clrs={'neu':'aaa','emp':'a22','fed':'45b','uni':'dc2'};

function parseState(id,current){
	var table=document.getElementById(id).parentNode.parentNode.cells[1].getElementsByTagName('TABLE');
	if(table) var done=Math.floor(msns[current]*parseInt(String(table[0].rows[0].cells[0].title).replace(/[^\d]/g,''),10)/100);
	return {'done':done,'left':msns[current]-done};
};

function addNode(node,rank,color){
	while(node.nodeName.toLowerCase()!='tbody'&&node.parentNode) node=node.parentNode;
	node.innerHTML+='<tr><td colspan="3" style="text-align:center;opacity:0.8;font-family:Verdana;font-size:10px;color:#'+color+'">'+rank.done+' done <span style="font-size:12px">››</span> '+rank.left+' left</td></tr>';
};

if(document.getElementById('competency_current')){
	var node=document.getElementById('competency_current');
	if(parseInt(node.alt,10)<=12) addNode(node,parseState('competency_current',parseInt(node.alt,10)),clrs['neu']);
}

if(document.getElementById('bar_milrank_current')){
	var node=document.getElementById('bar_milrank_current');
	var rank_match=String(node.src).match(/\/(emp|fed|uni)rank(\d+).png/i);
    if(rank_match.length==3&&parseInt(rank_match[2],10)<=12) addNode(node,parseState('bar_milrank_current',parseInt(rank_match[2],10)),clrs[rank_match[1]]);
}
